program miranda
use point_in_stl
implicit none
real ( kind = 8 ) qpt(3)
integer ( kind = 4 ) io

integer :: nx,ny,i,j
double precision, dimension(:,:), allocatable :: xgrid,ygrid,pip
double precision :: xmin,xmax,ymin,ymax,zloc
character(len=120) :: stl_file, out_file
character(len=32) :: arg
integer :: flipYZ

stl_file = 'naca0012.stl'
out_file = 'naca0012.tec'
nx = 2000
ny = 2000
xmin = -1
xmax = 2
ymin = -.2
ymax = .2
zloc = .1
flipYZ = 0


! Get CLI
IF (iargc() .ne. 9 .and. iargc().ne.10 ) THEN
   print*, 'Invalid arguments:'
   print*, './pistl stlfile x1 xn y1 yn nx ny outfile z1 [switchYZ]'
   STOP
END IF

! STL file
CALL getarg(1,arg)
stl_file = arg
print*, 'STLFILE:  ', TRIM(stl_file)

! Extents
CALL getarg(2,arg)
read(arg,*) xmin
CALL getarg(3,arg)
read(arg,*) xmax
print*, 'X-range:  ',xmin,xmax

CALL getarg(4,arg)
read(arg,*) ymin
CALL getarg(5,arg)
read(arg,*) ymax
print*, 'Y-range:  ',ymin,ymax

! Grid spacing
CALL getarg(6,arg)
read(arg,*) nx
CALL getarg(7,arg)
read(arg,*) ny
print*, 'Grid size:  ',nx,ny


! Outfile
CALL getarg(8,arg)
read(arg,*) out_file
print*, 'OUTFILE:  ', TRIM(out_file)

CALL getarg(9,arg)
read(arg,*) zloc
print*, 'Z-intersect:  ', zloc


if (iargc() .eq. 10) then
   CALL getarg(10,arg)
   read(arg,*) flipYZ
end if

if ( flipYZ == 1 ) then
   print*,'Notice: Flipping the Y/Z coordinates'
end if
   
!stop

allocate(xgrid(nx,ny))
allocate(ygrid(nx,ny))
allocate(pip(nx,ny))


do i=1,nx
   xgrid(i,:) = xmin + (xmax-xmin) / dble(nx) * dble(i-1)
end do

do i=1,ny
   ygrid(:,i) = ymin + (ymax-ymin) / dble(ny) * dble(i-1)
end do


!call point_in_stl_setup(TRIM(stl_file))
call piSTL_setup(TRIM(stl_file), .false. , .true. )


do i=1,nx
   do j=1,ny
      if (flipYZ == 1) then
         qpt = (/ xgrid(i,j), zloc, ygrid(i,j) /) 
      else
         qpt = (/ xgrid(i,j), ygrid(i,j), zloc /) 
      end if
      call point_in_stl_solver(qpt,io)
      pip(i,j) = io
   end do
end do

call piSTL_free

!Plotter
!============================================================================

open(200,file=TRIM(out_file))

write(200,*) 'variables= "x" , "y"  , "io"'
write(200,*) 'ZONE, I= ',nx,' J=',ny,' F=POINT'


 do j=1,ny
  do i=1,nx
     write(200,*) xgrid(i,j),ygrid(i,j), pip(i,j)
  enddo
enddo

close(200)





end program
