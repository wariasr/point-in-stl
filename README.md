# piSTL #
## *(pistol)* ##
## Point in STL fortran library for fast, lean, and accurate signed level-set surface queries. ##


### What does *piSTL* do? ###

* Bring up and allocate the library, precomputing for speed, with a given STL file.
* Make query call to the surfaces
* Take down the library
* Algorithm and slides [here.](https://bitbucket.org/wariasr/point-in-stl/downloads/main.pdf)


## How to use it ##

Setup the surface mesh
```
#!fortran
CALL piSTL_setup( 'myFile.stl' )
```

In your loops, call the function:
```
#!fortran
outside = piSTL_query( (/ x , y , z /) )
```

Free up memory
```
#!fortran
CALL piSTL_free()
```


### Who do I talk to? ###

* Walter Arias Ramirez
* Britton J. Olson